using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ballinhole : MonoBehaviour
{
    public GameObject wall;
    public Vector3 newPosition;
    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Ball")
        {
            wall.SetActive(false);
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
            other.transform.position = newPosition;
            other.gameObject.layer = 0;
            Debug.Log("Exit");
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
