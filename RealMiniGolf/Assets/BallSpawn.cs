using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawn : MonoBehaviour
{
    public GameObject ball;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "BatFollower(Clone)" || 
            other.gameObject.name == "BatFollower")
        {
            Rigidbody rb = ball.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            ball.transform.position = transform.position + new Vector3(1, 0, 0); 
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
