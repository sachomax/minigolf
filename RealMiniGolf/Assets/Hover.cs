using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour
{

    public Rigidbody target;
    public float hoverDistance;
    void Start()
    {

    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        if (Physics.Raycast(transform.position, Vector3.down, hoverDistance))
        {
            RaycastHit hitInfo;
            Physics.Raycast(transform.position, Vector3.down, out hitInfo, hoverDistance);
            float correction = hoverDistance - hitInfo.distance;
            target.MovePosition(new Vector3(target.position.x, target.position.y + correction, target.position.z));
        }
    }
}
