﻿using UnityEngine;

public class HoleTrigger : MonoBehaviour
{
    public GameObject wall;
    public Vector3 newPosition;
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Ball")
        {
            other.gameObject.layer = 9;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Ball")
        {
            wall.SetActive(false);
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
            other.transform.position = newPosition;
            other.gameObject.layer = 7;
        }
    }
}
